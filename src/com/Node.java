package com;

public class Node {
    private int value;
    private Node parent;
    private Node left;
    private Node right;

    Node(int value) {
        this.value = value;
    }

    public Node getLeft() {
        return left;
    }

    public Node setLeft(Node left) {
        left.setParent(this);
        this.left = left;
        return left;
    }

    public Node getRight() {
        return right;
    }

    public Node setRight(Node right) {
        right.setParent(this);
        this.right = right;
        return right;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
