package com;

public class Main {
    public static void main(String[] args) {
        Node root = new Node(5);
        Node lChild = root.setLeft(new Node(3));
        Node rChild = root.setRight(new Node(7));
        Node llChild = lChild.setLeft(new Node(2));
        Node lrChild = lChild.setRight(new Node(5));
        Node rlChild = rChild.setLeft(new Node(1));
        Node rrChild = rChild.setRight(new Node(0));
        Node rrlChild = rrChild.setLeft(new Node(2));
        Node rrrChild = rrChild.setRight(new Node(8));
        Node rrrrChild = rrrChild.setRight(new Node(5));

        TreeManager treeManager = new TreeManager();
        treeManager.getSubtreeInfo(rrChild);
    }

}
