package com;

import java.util.Collections;
import java.util.LinkedList;

public class TreeManager {
    private LinkedList<Integer> subtreeInt;
    private int subtreeSum;

    private void setSubtree(Node node) {
        subtreeInt = new LinkedList<>();
        pullSubtree(node);
    }

    public void getSubtreeInfo(Node node) {
        setSubtree(node);
        System.out.println("Subtree elements: " + subtreeInt.toString());
        System.out.println("Sum: " + getSubtreeSum());
        System.out.println("Average: " + getAverage());
        System.out.println("Median: " + getMedian());

    }

    private void pullSubtree(Node node) {
        subtreeInt.add(node.getValue());
        Node left = node.getLeft();
        Node right = node.getRight();
        if (left != null) {
            pullSubtree(left);
        }
        if (right != null) {
            pullSubtree(right);
        }
    }

    private int getSubtreeSum() {
        subtreeSum = subtreeInt.stream().mapToInt(Integer::intValue).sum();
        return subtreeSum;
    }

    private double getAverage() {
        return (double) subtreeSum / subtreeInt.size();
    }

    private double getMedian() {
        Collections.sort(subtreeInt);
        int tempIndex = subtreeInt.size() / 2;
        if (subtreeInt.size() % 2 != 0)
            return subtreeInt.get(tempIndex);
        else {
            int value = subtreeInt.get(tempIndex) + subtreeInt.get(tempIndex - 1);
            return (double) value / 2;
        }
    }
}
